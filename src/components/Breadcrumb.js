import { Link } from "react-router-dom";

const defaultValue = [
  {
    title: 'Home',
    path: '/' // null
  }
]

const Breadcrumb = ({ navigation = defaultValue }) => (
  <div className="container" style={{ marginTop: '80px' }}>
    <div className='breadcrumb'>
      {navigation.map((item, index) => (
        <span key={item.path}>
          {item.path ? <Link to={item.path}>{item.title}</Link> : <span>{item.title}</span>}
          {index !== navigation?.length - 1 && <span className="breadcrumb-separator">/</span>}
        </span>
      ))}
    </div>
  </div>
)

export default Breadcrumb;
