const Profile = () => {
  return <>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container" style={{ marginTop: '80px' }}>
        <div className='breadcrumb'>
          <span>
            <a href='/'>Home</a>
            <span className="breadcrumb-separator">/</span>
          </span>
          <span>
            <a href="/">My Profile</a>
          </span>
        </div>
      </div>
      <div className="container padding-32" id="contact" >
        <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
        <p>Lets get in touch and talk about your next project.</p>
        <form>
          <input className="input section border" type="text" placeholder="Name" name="name" />
          <div style={{ color: '#EF144A' }}>Name is required</div>
          <input className="input section border" type="text" placeholder="Email" name="email" />
          <div style={{ color: '#EF144A' }}>Email is required</div>
          <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} />
          <div style={{ color: '#EF144A' }}>Phone Number is required</div>
          <input className="input section border" type="password" placeholder="Password" name="password" />
          <div style={{ color: '#EF144A' }}>Password is required</div>
          <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword" />
          <div style={{ color: '#EF144A' }}>Retype Password is required</div>
          <button className="button black section" type="submit">
            <i className="fa fa-paper-plane" /> Update
          </button>
        </form>
      </div>
    </div>
  </>
}

export default Profile
