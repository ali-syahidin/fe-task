import React from 'react';

const Login = () => {
  return <>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container padding-32" id="contact" >
        <h3 className="border-bottom border-light-grey padding-16">Login</h3>
        <p>Lets get in touch and talk about your next project.</p>
        <form>
          <input className="input border" type="text" placeholder="Username" required name="username" />
          <input className="input section border" type="password" placeholder="Password" required name="Password" />
          <button className="button black section" type="submit">
            <i className="fa fa-paper-plane" />
            Login
          </button>
        </form>
      </div >
    </div>
  </>;
};

export default Login;
