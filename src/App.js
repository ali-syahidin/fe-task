import { Route, Routes } from 'react-router-dom'
import { SWRConfig } from 'swr'
import Home from 'pages/home'
import Shop from 'pages/shop'
import ShopDetail from 'pages/shop/detail'
import Layout from 'components/layout'
import Cart from 'pages/cart'
import Profile from 'pages/profile'
import Login from 'pages/login'
import 'assets/css/style.css'
import 'assets/css/custom.css'

const App = () => {
    return (
        <SWRConfig
            value={{
                fetcher: url => fetch(`https://api-tdp-2022.vercel.app${url}`, { mode: 'cors' }).then(res => res.json())
            }}
        >
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route index element={<Home />} />
                    <Route path="shop" element={<Shop />}>
                        <Route path=":shopId" element={<ShopDetail />} />
                    </Route>
                    <Route path="cart" element={<Cart />} />
                    <Route path="profile" element={<Profile />} />
                    <Route path="login" element={<Login />} />
                </Route>
            </Routes>
        </SWRConfig>
    )
}

export default App
