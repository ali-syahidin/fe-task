import useSWR from 'swr'
import { Link } from 'react-router-dom'
import banner from 'assets/img/architect.jpg'
import map from 'assets/img/map.jpg'

const Home = () => {
  const { data } = useSWR('/api/categories')

  return (<>
    <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
      <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
      <div className="display-middle center">
        <h1 className="xxlarge text-white">
          <span className="padding black opacity-min">
            <b>EDTS</b>
          </span>
          <span className="hide-small text-light-grey">Mart</span>
        </h1>
      </div>
    </header>

    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container padding-32" id="projects">
        <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
      </div>
      <div className="row-padding">
        {!data && (
          <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
            Loading . . .
          </div>
        )}
        {data && data?.data?.map(category => (
          <div key={category?.id} className="col l3 m6 margin-bottom">
            <Link to={`/shop?category=${category?.id}`}>
              <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                <div className="display-topleft black padding">{category?.name}</div>
                <img src={category?.image} alt={category?.name} style={{ maxWidth: '100%', minHeight: '100%' }} />
              </div>
            </Link>
          </div>
        ))}
      </div>

      <div className="container padding-32">
        <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
      </div>
    </div>
  </>)
}

export default Home
