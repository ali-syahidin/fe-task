export const formatRupiah = value => {
  const number = Number.isInteger(value) ? value : parseInt(value)
  return `Rp. ${number.toLocaleString('id')}`
}