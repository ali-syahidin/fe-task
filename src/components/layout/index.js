import { Outlet, Link } from "react-router-dom"

const Layout = () => {
  return <>
    <div className="top" >
      <div className="bar white wide padding card">
        <Link to="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</Link>
        <div className="right hide-small">
          <Link to="/" className="bar-item button">Home</Link>
          <Link to="/shop" className="bar-item button">Shop</Link>
          <Link to="/cart" className="bar-item button">My Cart</Link>
          <Link to="/profile" className="bar-item button">My Profile</Link>
          <Link to="/login" className="bar-item button">Login</Link>
        </div>
      </div>
    </div>
    <Outlet />
    <footer className="center black padding-16">
      <p>Copyright 2022</p>
    </footer>
  </>
}

export default Layout
