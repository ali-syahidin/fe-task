import Breadcrumb from "components/Breadcrumb"
import { formatRupiah } from "helper"
import { Link, useLocation } from "react-router-dom"
import useSWR from "swr"

const Shop = () => {
  const location = useLocation()
  const { data } = useSWR(`/api/products${location.search}`)

  return <>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <Breadcrumb
        navigation={[
          { title: 'Home', path: '/' },
          { title: 'Shop', path: null },
        ]}
      />
      <div className="container padding-32" id="about">
        <h3 className="border-bottom border-light-grey padding-16">
          All Product
        </h3>
      </div>
      <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
        {!data && (
          <div className="row-padding padding-large" style={{ height: '70vh', fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
            Loading . . .
          </div>
        )}
        {data?.data?.productList && data?.data?.productList?.map(product => (
          <div key={product?.id} className="product-card">
            {Boolean(product?.discount) && <div className="badge">Discount {product?.discount}%</div>}
            <div className="product-tumb">
              <img src={product?.image} alt={product?.title} />
            </div>
            <div className="product-details">
              <span className="product-catagory">{product?.category}</span>
              <h4>
                <Link to={`/shop/${product?.id}`}>{product?.title}</Link>
              </h4>
              <p>{product?.description}</p>
              <div className="product-bottom-details">
                <div className="product-price">{formatRupiah(product?.price)}</div>
                <div className="product-links">
                  <Link to={`/shop/${product?.id}`}>View Detail<i className="fa fa-heart"></i></Link>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  </>
}

export default Shop
