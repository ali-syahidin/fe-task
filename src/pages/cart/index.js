const Cart = () => {
  return <>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container" style={{ marginTop: '80px' }}>
        <div className='breadcrumb'>
          <span>
            <a href='/'>Home</a>
            <span className="breadcrumb-separator">/</span>
          </span>
          <span>
            <a href="/">My Cart</a>
          </span>
        </div>
      </div>

      <div className="cart-info">
        <div className="parent__left-side">
          <div className="shopping-cart">
            <div className="title">My Cart</div>
            <div className="flex-container item">
              <div className="buttons">
                <span className="delete-btn"></span>
                <span className="like-btn"></span>
              </div>
              <div className="image">
                <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="" style={{ width: '75px' }} />
              </div>
              <div className="description" style={{ width: '50%' }}>
                <span>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</span>
                <span>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</span>
              </div>
              <div className="quantity" style={{ width: '25%', textAlign: 'center' }}>
                <button className="minus-btn" type="button" name="button">
                  <img src="https://designmodo.com/demo/shopping-cart/minus.svg" alt="" />
                </button>
                <input type="text" name="name" value="4" readOnly />
                <button className="plus-btn" type="button" name="button">
                  <img src="https://designmodo.com/demo/shopping-cart/plus.svg" alt="" />
                </button>
                <div className="description"><span>Stock : 2</span></div>
              </div>
              <div className="total-price" style={{ width: '15%' }}>
                <div style={{ textDecoration: 'line-through', fontSize: '14px' }}>
                  Rp 250.000
                </div>
                <div style={{ fontWeight: 800 }}>
                  Rp 225.000
                </div>
              </div>
            </div>
            <div className="flex-container item">
              <div className="buttons">
                <span className="delete-btn"></span>
                <span className="like-btn"></span>
              </div>
              <div className="image">
                <img src="https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg" alt="" style={{ width: '75px' }} />
              </div>
              <div className="description" style={{ width: '50%' }}>
                <span>Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops</span>
                <span>Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday</span>
              </div>
              <div className="quantity" style={{ width: '25%', textAlign: 'center' }}>
                <button className="minus-btn" type="button" name="button">
                  <img src="https://designmodo.com/demo/shopping-cart/minus.svg" alt="" />
                </button>
                <input type="text" name="name" value="4" readOnly />
                <button className="plus-btn" type="button" name="button">
                  <img src="https://designmodo.com/demo/shopping-cart/plus.svg" alt="" />
                </button>
                <div className="description"><span>Stock : 2</span></div>
              </div>
              <div className="total-price" style={{ width: '15%' }}>
                <div style={{ textDecoration: 'line-through', fontSize: '14px' }}>
                  Rp 250.000
                </div>
                <div style={{ fontWeight: 800 }}>
                  Rp 225.000
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="parent__right-side">
          <div className='price-summary-cart'>
            <div className='price-summary-body'>
              <div>
                <h6 className="price-summary-title">Shopping Summary</h6>
                <div className="price-summary-detail">
                  <p className="left">Total Price (items)</p>
                  <p className="right">Rp 250.000</p>
                </div>
                <div className="price-summary-detail">
                  <p className="left">Total Discount (items)</p>
                  <p className="right">Rp 25.000</p>
                </div>
                <hr className="css-1u5v5t"></hr>
                <div className="price-summary-detail">
                  <h6 className="left">GrandTotal</h6>
                  <h6 className="right">Rp 225.000</h6>
                </div>
                <button className='button light-grey block'>
                  Order
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="cart-info">
        <div className="parent__left-side">
          <div className="shopping-cart">
            <div className="title">My Cart</div>
            <div style={{ textAlign: 'center', margin: '150px', fontWeight: 800 }}>Your Cart is Empty</div>
          </div>
        </div>
        <div className="parent__right-side">
          <div className='price-summary-cart'>
            <div className='price-summary-body'>
              <div>
                <h6 className="price-summary-title">Shopping Summary</h6>
                <div className="price-summary-detail">
                  <p className="left">Total Price (items)</p>
                  <p className="right">Rp 250.000</p>
                </div>
                <div className="price-summary-detail">
                  <p className="left">Total Discount (items)</p>
                  <p className="right">Rp 25.000</p>
                </div>
                <hr className="css-1u5v5t"></hr>
                <div className="price-summary-detail">
                  <h6 className="left">GrandTotal</h6>
                  <h6 className="right">Rp 225.000</h6>
                </div>
                <button className='button light-grey block'>
                  Order
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
}

export default Cart
