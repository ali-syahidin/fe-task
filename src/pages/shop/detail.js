const ShopDetail = () => {
  return <>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container" style={{ marginTop: '80px' }}>
        <div className='breadcrumb'>
          <span>
            <a href='/'>Home</a>
            <span className="breadcrumb-separator">/</span>
          </span>
          <span>
            <a href="/">Shop</a>
            <span className="breadcrumb-separator">/</span>
            <a href="/">Detail</a>
          </span>
        </div>
      </div>
      <div className="container padding-32" id="about">
        <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
      </div>

      <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
        <div className="col l3 m6 margin-bottom">
          <div className="product-tumb">
            <img src="https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg" alt="Product 1" />
          </div>
        </div>
        <div className="col m6 margin-bottom">
          <h3>Mens Casual Premium Slim Fit T-Shirts</h3>
          <div style={{ marginBottom: '32px' }}>
            <span>Category : <strong>Men's Clothing</strong></span>
            <span style={{ marginLeft: '30px' }}>Review : <strong>4.8</strong></span>
            <span style={{ marginLeft: '30px' }}>Stock : <strong>10</strong></span>
            <span style={{ marginLeft: '30px' }}>Discount : <strong>0 %</strong></span>
          </div>
          <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
            Rp. 250.000
          </div>
          <div style={{ marginBottom: '32px' }}>
            Slim-fitting style, contrast raglan long sleeve, three-button henley placket, light weight & soft fabric for breathable and comfortable wearing. And Solid stitched shirts with round neck made for durability and a great fit for casual fashion wear and diehard baseball fans. The Henley style round neckline includes a three-button placket.
          </div>
          <div style={{ marginBottom: '32px' }}>
            <div><strong>Quantity : </strong></div>
            <input type="number" className="input section border" name="total" placeholder='Quantity' />
          </div>
          <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
            Sub Total : Rp. 250.000
            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. 350.000</strong></span>
          </div>
          <button className='button light-grey block' >Add to cart</button>
          <button className='button light-grey block' disabled={true}>Empty Stock</button>
        </div>
      </div>
    </div>
  </>
}

export default ShopDetail
